# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'dc4b5edba380e81cf126ef70984b448873ba409f12d2c3631a501567ac9b368410727dc84b830adec1a213e07896617160d83a1e99f4ce4eb7c1736c9ea5bbb1'
